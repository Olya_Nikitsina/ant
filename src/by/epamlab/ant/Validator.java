package by.epamlab.ant;

import java.io.File;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;
import java.util.regex.Pattern;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.ProjectHelper;
import org.apache.tools.ant.Target;
import org.apache.tools.ant.Task;
 
public class Validator extends Task { 
	
    private static final String TARGET_PATTERN = "[[A-Za-z]-]*";
    private static final String DEFAULT_TARGET = "main";
	private boolean check_depends;
	private boolean check_names;
	private boolean check_defaults;
	private boolean check_names_flag;
	private boolean check_depends_flag;
	private String default_target;
	private String target_name;
	private Enumeration<String> dependencies;

	public void execute() throws BuildException { 
		
		for (Iterator<Buildfile> iterator = buildfile.iterator(); iterator.hasNext();) {
			
			Buildfile path = (Buildfile) iterator.next();
			
//where this task is used?			
			String location = path.getLocation();
			
			Project project = new Project();
			File file = new File(location);

			ProjectHelper projectHelper = ProjectHelper.getProjectHelper();
			projectHelper.configureProject(project, file);
			
			System.out.println("buildfile: " + location);
			
//check_depends - if targets with depends are used instead of "main" point
			if (check_depends) {
				dependencies = project.getTargets().get(DEFAULT_TARGET).getDependencies();
				check_depends_flag = false;
				
				while (dependencies.hasMoreElements()) {
					check_depends_flag = true;
					break;
				}
				
				if(check_depends_flag) {
					System.out.println("targets with depends are used instead of 'main' point");
				} else {
					System.out.println("targets with depends are not used instead of 'main' point");
				}
			}
			
//check_defaults - check default target
			if (check_defaults) {
				default_target = project.getDefaultTarget();
				if (default_target != null) {
					System.out.println("project contains default target");
				} else {
					System.out.println("project not contains default target");
				}
			}
			
//check_names - if names contains only letters with '-'
			if (check_names) {
				Hashtable<String, Target> targets = project.getTargets();
				Enumeration<Target> targets_elem = targets.elements();
				check_names_flag = true;
				
				while (targets_elem.hasMoreElements()) {
					Target target = (Target) targets_elem.nextElement();
					target_name = target.getName();
					if (!Pattern.matches(TARGET_PATTERN, target_name)) {
						check_names_flag = false;
						break;
					}
				}
				
				if (check_names_flag) {
					System.out.println("names contains only letters with '-'");
				} else {
					System.out.println("names not contains only letters with '-'");
				}
			}
		}
	} 

	public void setCheckdepends(boolean checkdepends) { 
		this.check_depends = checkdepends; 
	} 
	public void setCheckdefault(boolean checkdefaults) { 
		this.check_defaults = checkdefaults; 
	}
	public void setChecknames(boolean checknames) { 
		this.check_names = checknames; 
	}
	
	Vector<Buildfile> buildfile = new Vector<Buildfile>();
	
	public Buildfile createBuildfile() {
		Buildfile location = new Buildfile();
		buildfile.add(location);
		return location;
	}
	
	public class Buildfile {
		
		public Buildfile() {}
		private String location;
		public void setLocation(String location) {
			this.location = location;
		}
		public String getLocation() {
			return location;			
		}
	}
}